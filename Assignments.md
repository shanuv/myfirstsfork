# Assignments for this course.

## Instructions

1. You need to create a separate private repository for each of the
   assignment. Please call these assignment1, assignment2, etc.

2. Each student is assigned to some TA. You need give read access to
   your TA and no one else so that he/she can clone your repo and
   evaluate it. You can find bitbucket usernames of all TAs in
   [List of TA][ta].

3. For each assignment, please make a set of small commits instead of
   one single commit. Each logical stuff you do can be in a separate
   commit.  The commit messages act as a documentation on history of
   the project and hence having good logical commit messages are
   important for the project.

4. If there is any clarifications/mistakes please use the issue tracker.

#### Repository Structure
```
<your repository>
    |
    |-- assignment1
    |       |-- <other files>
    |       |-- README.md
    |
    |--
    |
    |-- assignment5
    |       |-- <other files>
    |       |-- README.md
    |
    |--
```

## Assignment 1

**Deadline: 25th Jan 2014 (Sunday) midnight**


The goal of this assignment is to get you familiar with latex and some
of its features.

1. Create your resume.
    * Add basic information
    * Add a passport size photograph
    * Add your transcript in tabular format. Please follow the format
      that DOAA uses. The grades and subjects taken can be fictitious but
      the format should be that of DOAA.

2. Write an article on any *one* of the following
    * Define AP and GP and prove its summation formula.
    * Define the Harmonic number and Eulers integral representation of it.
      <http://en.wikipedia.org/wiki/Harmonic_number>

   In each case you need to give a reference to a book or url. This should
   use bibtex.

[ta]: https://bitbucket.org/ppk-teach/tools/wiki/List%20of%20TA
