# Programming Tools and Techniques.

This repository will contain all the material relevant for the
Programming tools and techniques course.


## How to contribute?

You can fork this repository on bitbucket and send pull request. Make
sure that your patches have same author name and email id. You can set
these once and for all using the following command:

```bash
$ git config --global user.name "Piyush Kurur"
$ git config --global user.email ppk@myhost.mydomain

```

Also make sure that your patches do not have trailing spaces and
newlines. Easy to get this operational is to set your pre-commit hook.
This will disallow you from committing patches that have a trailing
space or trailing set of newlines.

```bash

$ mv .git/hooks/pre-commit.sample .git/hooks/pre-commit

```

## Sending clean pull requests.

I often get pull requests where there are a lot of intermediate merges
from my master. While reviewing this is some times irritating. I suggest
that you follow these steps to reduce unwanted merges.


1. Before starting to hack get the latest from the upstream

	$ git pull upstream # Or whatever is the name that you gave to
	                    # my master repository.

2. Avoid sending patches from you master to my master branch. Lot of
   the spurious merges comes because you are hacking on the master and
   pull in changes from my master.

3. Create a separate branch, hack the changes, and send pull request
   from that branch.

       $ git checkout -b topic # Create and checkout to the branch topic
                               # The name topic is not god given. Give
							   # the branch some suitable name.
       $ hack hack
	   $ git push origin topic # push to your bitbucket clone and pull
	                           # request.
